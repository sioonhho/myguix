(define-module (myguix packages xdisorg)
  #:use-module ((gnu packages xdisorg) #:prefix gnu:)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:export (libdrm))

(define libdrm
  (package
    (inherit gnu:libdrm)
    (name "libdrm")
    (version "2.4.114")
    (source
      (origin
        (method url-fetch)
        (uri (string-append
               "https://dri.freedesktop.org/libdrm/libdrm-"
               version ".tar.xz"))
        (sha256
          (base32 "09nhk3jx3qzggl5vyii3yh4zm0npjqsbxhzvxrg2xla77a2cyj9h"))))))

; vim:ts=2 sw=2 sts=2 et

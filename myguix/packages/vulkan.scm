(define-module (myguix packages vulkan)
  #:use-module (gnu packages check)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module ((gnu packages vulkan) #:prefix gnu:)
  #:use-module (gnu packages xorg)
  #:use-module (guix build-system cmake)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:export (vulkan-headers
            vulkan-loader))

(define vulkan-headers
  (package
    (inherit gnu:vulkan-headers)
    (name "vulkan-headers")
    (version "1.2.203")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/KhronosGroup/Vulkan-Headers")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0np4xjvwkxc4dzcjb80w1hjnc6bxy9d98awxlw1fs16xhy4bgkzq"))))))

(define vulkan-loader
  (package
    (inherit gnu:vulkan-loader)
    (name "vulkan-loader")
    (version "1.2.203")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/KhronosGroup/Vulkan-Loader")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1ixb8s3ink8njx2ip55im4jf0sj2n8bsq5144qdg5mbk9ckhym8y"))))
    (build-system cmake-build-system)
    (arguments
     `(#:configure-flags
       ,#~(list
            (string-append "-DVULKAN_HEADERS_INSTALL_DIR="
                           #$(this-package-input "vulkan-headers"))
            (string-append "-DGOOGLETEST_INSTALL_DIR="
                           (getcwd) "/source/external/googletest")
            "-DBUILD_TESTS=ON")
        #:phases (modify-phases %standard-phases
                   (add-after 'unpack 'unpack-googletest
                     (lambda* (#:key inputs #:allow-other-keys)
                       (let ((gtest (assoc-ref inputs "googletest:source")))
                         (when gtest
                           (copy-recursively gtest "external/googletest"))
                         #t))))))
    (native-inputs
     `(("googletest:source" ,(package-source googletest))
       ("libxrandr" ,libxrandr)
       ("pkg-config" ,pkg-config)
       ("python" ,python)
       ("wayland" ,wayland)))
    (inputs
      (list vulkan-headers))))

; vim:ts=2 sw=2 sts=2 et

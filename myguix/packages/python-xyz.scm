(define-module (myguix packages python-xyz)
  #:use-module (gnu packages check)
  #:use-module (gnu packages python-xyz)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:export (python-dasbus
            python-pam
            python-thefuzz))

(define python-dasbus
  (package
    (name "python-dasbus")
    (version "1.7")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/rhinstaller/dasbus")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1dii0skg7pqi4jhy1dkshqmn49i5ngq92sw7w7j4sxd3g8pjhj46"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f))
    (home-page "https://github.com/rhinstaller/dasbus")
    (synopsis "DBus library in Python 3")
    (description
      "This package provides a DBus library in Python 3.")
    (license license:lgpl2.1)))

(define python-pam
  (package
    (name "python-pam")
    (version "2.0.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/FirefighterBlu3/python-pam")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "05fvf9rg4h7sigzvipm1ghq34zk6i8na7wkyk905nvk4gdhln7ri"))))
    (build-system pyproject-build-system)
    (arguments
     `(#:tests? #f))
    (home-page "https://github.com/FirefighterBlu3/python-pam")
    (synopsis "Python pam module supporting Python 3")
    (description
      "This package provides a Python pam module supporting Python 3.")
    (license license:expat)))

(define python-thefuzz
  (package
    (name "python-thefuzz")
    (version "0.19.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/seatgeek/thefuzz")
               (commit version)))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0z7apb0bcp77zw6i3b9bw1abpqa94s57wyfbk0hnp3qa6bkqlhcj"))))
    (build-system python-build-system)
    (inputs
     (list python-levenshtein))
    (native-inputs
      (list python-hypothesis
            python-pycodestyle
            python-pytest))
    (home-page "https://github.com/seatgeek/thefuzz")
    (synopsis "Fuzzy String Matching in Python")
    (description
      "Fuzzy string matching like a boss. It uses Levenshtein Distance
to calculate the differences between sequences in a simple-to-use package.")
    (license license:gpl2)))

; vim:ts=2 sw=2 sts=2 et

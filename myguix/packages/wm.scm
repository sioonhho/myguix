(define-module (myguix packages wm)
  #:use-module (myguix packages python-xyz)
  #:use-module ((myguix packages vulkan) #:prefix myguix:)
  #:use-module ((myguix packages xdisorg) #:prefix myguix:)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages image)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages video)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xorg)
  #:use-module (guix build-system python)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:export (newm
            pywm))

(define newm
  (package
    (name "newm")
    (version "0.3alpha")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/jbuchermn/newm")
               (commit "40c1af12de990d07a9e3fe7f9d07e9efba31dc89")))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0jx5d8gb24fpnssi5wd53czxsmb0mapz96d83yqcqm8nzglm0l4p"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f))
    (inputs
      (list pywm))
    (native-inputs
      (list python-dasbus
            python-evdev
            python-imageio
            python-numpy
            python-thefuzz
            python-psutil
            python-pycairo
            python-pyfiglet
            python-pygobject
            python-pam
            python-six))
    (propagated-inputs
      (list linux-pam))
    (home-page "https://github.com/jbuchermn/newm")
    (synopsis "newm wayland compositor")
    (description "newm is a Wayland compositor written with laptops and touchpads in mind.")
    (license license:expat)))

(define pywm
  (package
    (name "pywm")
    (version "0.3alpha")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/jbuchermn/pywm")
               (commit "f20828d8c29dc430cf6d1e59e4fdf92dc3caf36a")
               (recursive? #t)))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1i474ccbxnmdiicvzbbfblp2i0jsj2r2d0pp5hgprk3yqgbgn9d2"))))
    (build-system python-build-system)
    (inputs
      (list ffmpeg
            glslang
            myguix:libdrm
            libinput-minimal
            libpng
            libseat
            libxkbcommon
            mesa
            ninja
            pixman
            python-evdev
            python-imageio
            python-numpy
            python-pycairo
            wayland
            wayland-protocols-next
            myguix:vulkan-loader
            xcb-util-errors
            xcb-util-renderutil
            xcb-util-wm
            xorg-server-xwayland))
    (native-inputs
      (list cmake
            meson
            pkg-config))
    (home-page "https://github.com/jbuchermn/pywm")
    (synopsis "pywm is an abstraction layer fo newm encapsulating all c code")
    (description
      "wayland compositor core employing wlroots - aims to handle the actual
layout loginc in python thereby enabling easily accessible wm concepts.")
    (license license:expat)))

; vim:ts=2 sw=2 sts=2 et
